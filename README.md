# libreoffice-mtheme
`libreoffice-mtheme` is an adaptation of the [Metropolis](https://github.com/matze/mtheme) LaTeX-Beamer-Theme by Matthias Vogelgesang for LibreOffice Impress.
All credit goes to him for creating the original theme.

## Installation
Simply import the `.otp` file using the Import Template dialog when creating a new Impress presentation.

# License
`libreoffice-mtheme` is licensed under a [Creative Commons Attribution-ShareAlike International 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license. This does not affect presentations created with this theme.